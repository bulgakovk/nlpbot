"use strict";

const request = require('request');
const client_access_token = "ba531c42535c4e6fb69a420651949be5";

module.exports = app => {
	app.post('/message', (req, res) => {
		let message = req.body.message || "";
		let query = "https://api.api.ai/v1/query?v=20150910&query=" + message + "&lang=en";
		let options = {
			host: query,
			port: "",
			method: "GET"
		};

		let requestP = new Promise((resolve, reject) => {
			console.log("Requesting...");
			request.get({
				url: query,
				auth: {
					'bearer': client_access_token
				}
			}, (err, response) => {
				if (err) reject(err);
				resolve(response);
			});
		});

		requestP.then(resp => {
			let response = JSON.parse(resp.body);
			console.log(response.result.parameters);
			res.json(resp);
		}).catch(err => {
			console.log(err);
		});
	});
};
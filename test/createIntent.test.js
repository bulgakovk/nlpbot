"use strict";

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../src/app.js');
const async = require('async');

chai.use(chaiHttp);

const askTimePhrase = "When you want to start your trip?";

describe('Use cases', () => {

	it("Simple confirmation with 1,2 from start", (done) => {
		let one = (cb) => {
			chai.request(server)
				.post('/bot')
				.send({
					id: 1,
					query: "create from V.O. 8th line to Nevsky 21312321"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("errorMessage").eql(null);
					res.body.should.have.property("result").eql(askTimePhrase);

					cb();
				});
		};

		let two = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id: 1,
					query: "in 11 pm"
				})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("errorMessage").eql(null);
					res.body.should.have.property("result").eql("Let me confirm your trip. You are going to drive from V.O. 8th line to Nevsky 21312321 at 23:00:00. That's correct?");

					cb();
				});
		};

		let three = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id: 1,
					query: "+"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("errorMessage").eql(null);
					res.body.should.have.property("isConfirmed").eql(true);
					res.body.result.should.have.property("type").eql("CREATE");
					res.body.result.params.time.should.have.property("value").eql("23:00:00");
					res.body.result.params.pointB.should.have.property("value").eql("Nevsky 21312321");

					cb();
					done();
				});
		};

		async.series([
			one, two, three
		]);

	});

	it("1,2 params from start, Declining then yes/yes/yes", (done) => {
		let one = (cb) => {
			chai.request(server)
				.post('/bot')
				.send({
					id : 2,
					query : "from Nevsky 1 to V.O. 8 lane"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("result").eql(askTimePhrase);

					cb();
				});
		};

		let two = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id    : 2,
					query : "at 5 pm"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("result").eql("Let me confirm your trip. You are going to drive from Nevsky 1 to V.O. 8 lane at 17:00:00. That's correct?");

					cb();
				});
		};

		let three = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id : 2,
					query : "no"
				})
				.end( (err, res) => {
					res.body.should.have.property("result").eql("Are you going start trip from Nevsky 1?");

					cb();
				});
		};

		let four = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id    : 2,
					query : "+"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("result").eql("Are you going to move on V.O. 8 lane?");

					cb();
				});
		};

		let five = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id    : 2,
					query : "+"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("result").eql("Are you going to start your trip at 17:00:00?");

					cb();
				});
		};

		let six = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id    : 2,
					query : "+"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("isConfirmed").eql(true);

					cb();
					done();
				});
		};

		async.series([
			one, two, three, four, five, six
		]);
	 });


	it("Query with all parameters and confirming", (done) => {
		let one = (cb) => {
			chai.request(server)
				.post('/bot')
				.send({
					id: 3,
					query: "create from V.O. 8th line to Nevsky 21312321 at 10 am"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("errorMessage").eql(null);
					res.body.should.have.property("result").eql("Let me confirm your trip. " +
						"You are going to drive from V.O. 8th line to Nevsky 21312321 at 10:00:00. " +
						"That's correct?");

					cb();
				});
		};

		let second = (cb) => {
			chai.request(server)
				.put('/bot')
				.send({
					id: 3,
					query: "aga"
				})
				.end( (err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("status").eql("SUCCESS");
					res.body.should.have.property("errorMessage").eql(null);
					res.body.should.have.property("isConfirmed").eql(true);
					res.body.result.params.time.should.have.property("value").eql("10:00:00");

					cb();
					done();
				});
		};

		async.series([one, second])
	});

});
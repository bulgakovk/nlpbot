"use strict";
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

//App Configs
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
	extended: true
}));

//Enable bot API
require('./api/api.js')(app);

app.listen(8080);

module.exports = app;

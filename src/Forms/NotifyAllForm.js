const Form = require("./Form");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer");
const confirmationAttempts = 3;
const Request = require("../AI/Request.js");

class NotifyAllForm extends Form {
	constructor(entitites, query) {
		super();
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "NotifyAll"; //For REST API using
		this.processEntities(entitites, query);
	}

	fillParameter(param) {
		return new Promise( (resolve, reject) => {
			let expectedType = this.getExpectedType(param);
			this.isLastQuestionDeclining = false;
			this.isLastQuestionAgreement = false;

			switch (expectedType) {
				case "ID" :
					if (typeof param === "number") {
						this.params.ID.value = param;
					}
					if (LexAnalyzer.doesContainsNumber(param)) {
						this.params.ID.value = LexAnalyzer.returnNumberFromString(param);
					}

					resolve();
					break;

				case "message" :
					if (LexAnalyzer.isValidString(param)) {
						this.params.message.value = param;
					}

					resolve();
					break;

				case "Confirmation" :
					if (LexAnalyzer.isAgreement(param)) {
						if (!this.isInDeclineningState) {
							this.isConfirmed = true;
						} else {
							this.isLastQuestionAgreement = true;
						}
					} else if (LexAnalyzer.isDeclination(param)) {
						if (this.isInDeclineningState) {
							this.isLastQuestionDeclining = true;
						} else {
							this.isInDeclineningState = true;
						} // If we're not in declining state -> swith to that state
					}

					resolve();
			}
		});
	}

	processEntities(entities, query) {
		if (LexAnalyzer.doesContainID(query)) {
			this.params.ID.value = LexAnalyzer.returnID(query);
			query = query.replace(/\b\d+\b/, "");
			query = query.replace(/\bnotify all\b|\bnotifyall\b|\bnotify_all\b|notify/, "");
			query = LexAnalyzer.tryToDeleteAllSpaceSymbols(query);
			if (query.length) {
				this.params.message.value = query;
			}
		} else {
			query = query.replace(/\bnotify all\b|\bnotifyall\b|\bnotify_all\b|\bnotify\b/, "");
			query = LexAnalyzer.tryToDeleteAllSpaceSymbols(query);

			if (query.length) {
				this.params.message.value = query;
			}
		}
	}

	generateConfirming() {
		return "Let me confirm your notifying. Are you going to notify users about trip #" + this.params.ID.value +
			" with message: " + this.params.message.value + ". That's correct?";
	}

	refreshContext() {
		this.params = {
			ID: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "About which trip you are going to notify?",
				phraseToConfirm : "Are you going to notify all about trip #"
			},
			message: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "Which message do you want to send?",
				phraseToConfirm : "Are you going to send message: "
			}
		};
		this.isConfirmed = false;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}
}

module.exports = NotifyAllForm;
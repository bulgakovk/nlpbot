const Form = require("./Form");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer");
const confirmationAttempts = 3;

class MyTripsForm extends Form {
	constructor(entitites, query) {
		super();
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "MYTRIPS"; //For REST API using
	}

	refreshContext() {
		this.params = {};
		this.isConfirmed = true;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}
}

module.exports = MyTripsForm;
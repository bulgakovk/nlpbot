const CreateForm = require("./CreateForm");
const JoinForm   = require("./JoinForm");
const CancelForm = require("./CancelForm");
const MyTripsForm = require("./MyTripsForm");
const PickMeUpForm = require("./PickMeUpForm");
const DelayForm = require("./DelayForm");
const InformForm = require("./InformForm");
const NotifyAll = require("./NotifyAllForm");

class FormBuilder {
	static buildForm(type, entitites, query) {
		switch (type) {
			case "CREATE" :
				return new CreateForm(entitites);
				break;

			case "JOIN" :
				return new JoinForm(entitites);
				break;

			case "CANCEL" :
				return new CancelForm(entitites);
				break;

			case "MYTRIPS" :
				return new MyTripsForm();
				break;

			case "PICK" :
				return new PickMeUpForm(entitites, query);
				break;

			case "DELAY" :
				return new DelayForm(entitites, query);
				break;

			case "INFORM" :
				return new InformForm(entitites, query);
				break;

			case "NOTIFY_ALL" :
				return new NotifyAll(entitites, query);
				break;
		}
	}
}

module.exports = FormBuilder;
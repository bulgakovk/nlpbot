const Form = require("./Form");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer");
const Request = require("../AI/Request.js");
const confirmationAttempts = 3;

class PickMeUpForm extends Form {
	constructor(entitites, query) {
		super();
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "PickMeUp"; //For REST API using
		this.processEntities(entitites, query);
	}

	fillParameter(param) {
		return new Promise( (resolve, reject) => {
			let expectedType = this.getExpectedType(param);
			this.isLastQuestionDeclining = false;
			this.isLastQuestionAgreement = false;

			switch (expectedType) {
				case "pointB" :
					if (LexAnalyzer.isValidString(param) ||
						(this.isInDeclineningState && LexAnalyzer.isValidString(param))) {
						this.processEntities(param);
					} else this.confirmationNumber++;

					resolve();
					break;

				case "time" :
					Request(param)
						.then( (response) => {
							let body = JSON.parse(response.body);

							if (body.result.metadata.intentName === "TIME") {
								this.params.time.value = body.result.parameters.Time;
							} else this.confirmationNumber++;

							resolve();
						})
						.catch(err => {
							console.log(err);
						});

				break;

				case "Confirmation" :
					if (LexAnalyzer.isAgreement(param)) {
						if (!this.isInDeclineningState) {
							this.isConfirmed = true;
						} else {
							this.isLastQuestionAgreement = true;
						}
					} else if (LexAnalyzer.isDeclination(param)) {
						if (this.isInDeclineningState) {
							this.isLastQuestionDeclining = true;
						} else {
							this.isInDeclineningState = true;
						} // If we're not in declining state -> swith to that state
					}

					resolve();
			}
		});
	}

	processEntities(entities, query) {
		if (entities.constructor === Array && entities.length != 0) {
			this.params.time.value = entities[0];
		} else {
			if (LexAnalyzer.doesContainPointB(query)) {
				this.crop(LexAnalyzer.returnPointB(query));
			}
		}
	}

	generateConfirming() {
		return "Let me confirm your trip. Are you going to move on " + this.params.pointB.value + ". That's correct?";
	}

	refreshContext() {
		this.params = {
			pointB: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "What is your destination?",
				phraseToConfirm : "Are you going to move on "
			},
			time : {
				value: null,
				isConfirmed: false,
				phraseToAsk: "When you are going to move?",
				phraseToConfirm : "Is that time correct: "
			}
		};
		this.isConfirmed = false;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}

	crop(entity) {
		//If addresses come from RegExp
		//We need to delete from/to/on
		let index = -1;
		if (index = entity.indexOf("on") != -1) {
			this.params.pointB.value = entity.substr(index + "on".length);
		} else if (index = entity.indexOf("to") != -1) {
			this.params.pointB.value = entity.substr(index + "to".length);
		} else this.params.pointB.value = entity;

		return;
	}
}

module.exports = PickMeUpForm;
const Request = require("../AI/Request.js");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer.js");
const confirmationAttempts = 3;

/*

*/

class CreateForm {
	constructor(entitites, query) {
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "CREATE"; //For REST API using
		this.processEntities(entitites, query);
	}

	processEntities(entities, query) {
		if (entities.constructor === Array) {
			this.entities.forEach((entity) => {
				this.crop(entity);
			});
		} else {
			this.crop(entities);
		}
	}

	generatePhrase() {
		if (confirmationAttempts === this.confirmationNumber) {
			return "I see we can't speak the same language. Do you want to reset context?";
		}
		let returnResult = null;
		//Это будет первая фраза после полученного query
		returnResult = this.tryToAskFirstTime();
		if (returnResult) return returnResult;

		/*
		If user says YES and some params were declined before -> accept first declined param
		If all params will be confirmed -> this.isConfirmed = true and return in the next statement
		*/
		if (this.isInDeclineningState && this.isLastQuestionAgreement) {
			this.acceptFirstDeclinedParam();
		}

		/*
		 ToDo:
		 1) apply Reinforcement Learning
		 2) rewrite with pattern(?)
		 */
		if (this.isInDeclineningState && this.isLastQuestionDeclining) {
			returnResult = this.tryToReask();
			if (returnResult) return returnResult;
		}

		if (this.isInDeclineningState) {
			returnResult = this.tryToConfirm();
			if (returnResult) return returnResult;
		}

		if (this.isConfirmed) return this;

		return this.generateConfirming(); // otherwise
	}

	generateConfirming() {
		return "Let me confirm your trip. You are going to drive from " +
				this.params.pointA.value + " to " + this.params.pointB.value + " at " + this.params.time.value + ". That's correct?";
	}

	/*
		Returns promise because we need to async execute
		a request to API.AI in some cases (for instance, if it's time)
	*/
	fillParameter(param) {
		return new Promise( (resolve, reject) => {
			let expectedType = this.getExpectedType(param);
			this.isLastQuestionDeclining = false;
			this.isLastQuestionAgreement = false;

			switch (expectedType) {
				case "pointA" :
					if (LexAnalyzer.isPointA(param) ||
						(this.isInDeclineningState && LexAnalyzer.isValidString(param))) {
						this.processEntities(param);
					} else this.confirmationNumber++;

					resolve();
					break;
				case "pointB" :
					if (LexAnalyzer.isPointB(param) ||
						(this.isInDeclineningState && LexAnalyzer.isValidString(param))) {
						this.processEntities(param);
					} else this.confirmationNumber++;

					resolve();
					break;
				case "time" :
					Request(param)
						.then( (response) => {
							let body = JSON.parse(response.body);

							if (body.result.metadata.intentName === "TIME") {
								this.params.time.value = body.result.parameters.Time;
							} else this.confirmationNumber++;

							resolve();
						})
						.catch(err => {
							console.log(err);
						});

					break;
				case "Confirmation" :
					if (LexAnalyzer.isAgreement(param)) {
						if (!this.isInDeclineningState) {
							this.isConfirmed = true;
						} else {
							this.isLastQuestionAgreement = true;
						}
					} else if (LexAnalyzer.isDeclination(param)) {
						if (this.isInDeclineningState) {
							this.isLastQuestionDeclining = true;
						} else {
							this.isInDeclineningState = true;
						} // If we're not in declining state -> swith to that state
					}

					resolve();
					//ToDo: reAsk questions
					break;
				case "Reset" :
					if (LexAnalyzer.isAgreement(param)) {
						this.refreshContext();
					} else this.confirmationNumber--;

					resolve();
					break;
			}
		});
	}

	getExpectedType(param) {
		if (this.confirmationNumber === confirmationAttempts) return "Reset";

		let returnResult = null;
		returnResult = this.getExpectedTypeName("value");
		if (returnResult) return returnResult;

		if (this.isInDeclineningState
			&& !LexAnalyzer.isAgreement(param)
				&& !LexAnalyzer.isDeclination(param)) {
			 	returnResult = this.getExpectedTypeName("isConfirmed");
				if (returnResult) return returnResult;
		}

		else return "Confirmation";
	}

	crop(entity) {
		//If addresses come from RegExp
		//We need to delete from/to/on
		let index = -1;
		if (index = entity.indexOf("from") != -1) {
			this.params.pointA.value = entity.substr(index + "from".length);
			return;
		} else if (index = entity.indexOf("on") != -1) {
			this.params.pointB.value = entity.substr(index + "on".length);
			return;
		} else if (index = entity.indexOf("to") != -1) {
			this.params.pointB.value = entity.substr(index + "to".length);
			return;
		}

		/*
		ToDo: добавить проверку являются ли они крректными
		 */
		if (this.getExpectedType() === "pointA") {
			this.params.pointA.value = entity;
		}
		if (this.getExpectedType() === "pointB") {
			this.params.pointB.value = entity;
		}
		if (this.getExpectedType() === "time") {
			this.params.time.value = entity;
		}
	}

	tryToAskFirstTime() {
		for (let param in this.params) {
			if (!this.params[param].value) {
				return this.params[param].phraseToAsk;
			}
		}
	}

	tryToReask() {
		for (let param in this.params) {
			if (!this.params[param].isConfirmed) {
				return this.params[param].phraseToAsk;
			}
		}
	}

	tryToConfirm() {
		for (let param in this.params) {
			if (!this.params[param].isConfirmed) {
				return this.params[param].phraseToConfirm + this.params[param].value + "?";
			}
		}
	}

	refreshContext() {
		this.params = {
			pointA: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "Where can I take you?",
				phraseToConfirm : "Are you going start trip from "
			},

			pointB: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "What's your destination?",
				phraseToConfirm : "Are you going to move on "
			},

			time: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "When you want to start your trip?",
				phraseToConfirm : "Are you going to start your trip at "
			}
		};
		this.isConfirmed = false;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}

	getExpectedTypeName(key) {
		for (let param in this.params) {
			if (!this.params[param][key]) {
				return param;
			}
		}
	}

	acceptFirstDeclinedParam() {
		this.isLastQuestionAgreement = false;
		let index=0; let amount = 0;
		for (let temp in this.params) {
			amount++;
		}

		for (let param in this.params) {
			index++;
			if (!this.params[param].isConfirmed) {
				this.params[param].isConfirmed = true;

				/*
				 In the last param check change
				 isDeclined -> false
				 and
				 isConfirmed -> true
				 because
				 we accept each param
				 */
				if (index === amount) {
					this.isConfirmed = true;
				}

				return; // return because we don't need to confirm more than 1 parameter
			}
		}
	}

}

module.exports = CreateForm;
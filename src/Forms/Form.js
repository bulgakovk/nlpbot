const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer.js");

const confirmationAttempts = 3;

class Form {
	tryToConfirm() {
		for (let param in this.params) {
			if (!this.params[param].isConfirmed) {
				return this.params[param].phraseToConfirm + this.params[param].value + "?";
			}
		}
	}

	acceptFirstDeclinedParam() {
		this.isLastQuestionAgreement = false;
		let index=0; let amount = 0;
		for (let temp in this.params) {
			amount++;
		}

		for (let param in this.params) {
			index++;
			if (!this.params[param].isConfirmed) {
				this.params[param].isConfirmed = true;

				/*
				 In the last param check change
				 isDeclined -> false
				 and
				 isConfirmed -> true
				 because
				 we accept each param
				 */
				if (index === amount) {
					this.isConfirmed = true;
				}

				return; // return because we don't need to confirm more than 1 parameter
			}
		}
	}

	getExpectedTypeName(key) {
		for (let param in this.params) {
			if (!this.params[param][key]) {
				return param;
			}
		}
	}

	tryToReask() {
		for (let param in this.params) {
			if (!this.params[param].isConfirmed) {
				return this.params[param].phraseToAsk;
			}
		}
	}

	tryToAskFirstTime() {
		for (let param in this.params) {
			if (!this.params[param].value) {
				return this.params[param].phraseToAsk;
			}
		}
	}

	getExpectedType(param) {
		if (this.confirmationNumber === confirmationAttempts) return "Reset";

		let returnResult = null;
		returnResult = this.getExpectedTypeName("value");
		if (returnResult) return returnResult;

		if (this.isInDeclineningState
			&& !LexAnalyzer.isAgreement(param)
			&& !LexAnalyzer.isDeclination(param)) {
			returnResult = this.getExpectedTypeName("isConfirmed");
			if (returnResult) return returnResult;
		}

		else return "Confirmation";
	}

	generatePhrase() {
		if (confirmationAttempts === this.confirmationNumber) {
			return "I see we can't speak the same language. Do you want to reset context?";
		}
		//If form doesn't contain params -> return form
		/*
		let length = 0;
		for (let param in this.params) {
			length++;
		}

		if (length === 0) return this;
		*/
		let returnResult = null;
		//Это будет первая фраза после полученного query
		returnResult = this.tryToAskFirstTime();
		if (returnResult) return returnResult;

		/*
		 If user says YES and some params were declined before -> accept first declined param
		 If all params will be confirmed -> this.isConfirmed = true and return in the next statement
		 */
		if (this.isInDeclineningState && this.isLastQuestionAgreement) {
			this.acceptFirstDeclinedParam();
		}

		/*
		 ToDo:
		 1) apply Reinforcement Learning
		 2) rewrite with pattern(?)
		 */
		if (this.isInDeclineningState && this.isLastQuestionDeclining) {
			returnResult = this.tryToReask();
			if (returnResult) return returnResult;
		}

		if (this.isInDeclineningState) {
			returnResult = this.tryToConfirm();
			if (returnResult) return returnResult;
		}

		if (this.isConfirmed) return this;

		return this.generateConfirming(); // otherwise
	}
}

module.exports = Form;
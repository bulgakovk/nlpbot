const Form = require("./Form");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer");
const confirmationAttempts = 3;

class JoinForm extends Form {
	constructor(entitites, query) {
		super();
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "JOIN"; //For REST API using
		this.processEntities(entitites, query);
	}

	fillParameter(param) {
		return new Promise( (resolve, reject) => {
			let expectedType = this.getExpectedType(param);
			this.isLastQuestionDeclining = false;
			this.isLastQuestionAgreement = false;

			switch (expectedType) {
				case "ID" :
					if (typeof param === "number") {
						this.params.ID.value = param;
					}
					if (LexAnalyzer.doesContainsNumber(param)) {
						this.params.ID.value = LexAnalyzer.returnNumberFromString(param);
					}

					resolve();
					break;

				case "Confirmation" :
					if (LexAnalyzer.isAgreement(param)) {
						if (!this.isInDeclineningState) {
							this.isConfirmed = true;
						} else {
							this.isLastQuestionAgreement = true;
						}
					} else if (LexAnalyzer.isDeclination(param)) {
						if (this.isInDeclineningState) {
							this.isLastQuestionDeclining = true;
						} else {
							this.isInDeclineningState = true;
						} // If we're not in declining state -> swith to that state
					}

					resolve();
			}
		});
	}

	processEntities(entities, query) {
		this.params.ID.value = entities[0];
	}

	generateConfirming() {
		return "Let me confirm your joining. Are you going to join trip #" + this.params.ID.value + ". That's correct?";
	}

	refreshContext() {
		this.params = {
			ID: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "Which trip do you want to join?",
				phraseToConfirm : "Are you going to join trip #"
			}
		};
		this.isConfirmed = false;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}
}

module.exports = JoinForm;
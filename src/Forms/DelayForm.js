const Form = require("./Form");
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer");
const confirmationAttempts = 3;
const Request = require("../AI/Request.js");

class DelayForm extends Form {
	constructor(entitites, query) {
		super();
		// Defining properties
		this.entities = entitites;
		this.refreshContext();
		this.type = "DELAY"; //For REST API using
		this.processEntities(entitites, query);
	}

	fillParameter(param) {
		return new Promise( (resolve, reject) => {
			let expectedType = this.getExpectedType(param);
			this.isLastQuestionDeclining = false;
			this.isLastQuestionAgreement = false;

			switch (expectedType) {
				case "ID" :
					if (typeof param === "number") {
						this.params.ID.value = param;
					}
					if (LexAnalyzer.doesContainsNumber(param)) {
						this.params.ID.value = LexAnalyzer.returnNumberFromString(param);
					}

					resolve();
					break;

				case "time" :
					Request(param)
						.then( (response) => {
							let body = JSON.parse(response.body);

							if (body.result.metadata.intentName === "TIME") {
								this.params.time.value = body.result.parameters.Time;
							} else this.confirmationNumber++;

							resolve();
						})
						.catch(err => {
							console.log(err);
						});

					break;

				case "Confirmation" :
					if (LexAnalyzer.isAgreement(param)) {
						if (!this.isInDeclineningState) {
							this.isConfirmed = true;
						} else {
							this.isLastQuestionAgreement = true;
						}
					} else if (LexAnalyzer.isDeclination(param)) {
						if (this.isInDeclineningState) {
							this.isLastQuestionDeclining = true;
						} else {
							this.isInDeclineningState = true;
						} // If we're not in declining state -> swith to that state
					}

					resolve();
			}
		});
	}

	processEntities(entities, query) {
		if (entities[0]) this.params.time.value = entities[0];
		if (LexAnalyzer.doesContainID(query)) {
			this.params.ID.value = LexAnalyzer.returnID(query);
		}
	}

	generateConfirming() {
		return "Let me confirm your delaying. Are you going to start trip #" + this.params.ID.value +
			" at " + this.params.time.value + ". That's correct?";
	}

	refreshContext() {
		this.params = {
			ID: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "Which trip do you want to delay?",
				phraseToConfirm : "Are you going to delay trip #"
			},
			time: {
				value: null,
				isConfirmed: false,
				phraseToAsk: "When you want to start trip?",
				phraseToConfirm : "Are you going to this trip at "
			}
		};
		this.isConfirmed = false;
		this.isInDeclineningState = false; // When true -> means that we are in "confirming
		//each parameter state

		//To Reask form parameters
		this.isLastQuestionAgreement = false;
		this.isLastQuestionDeclining = false;

		this.confirmationNumber = 0;
	}
}

module.exports = DelayForm;
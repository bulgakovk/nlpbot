"use strict";

const request = require('request');

const client_access_token = "ff6078bf82f045de99916465fc02c763";

module.exports = (input) => {
	let query = "https://api.api.ai/v1/query?v=20150910&query=" + input
		+ "&lang=en" + "&sessionId=" + getTime();
	let options = {
		host : query,
		port : "",
		method : "GET"
	};

	return new Promise( (resolve, reject) => {
		request.get({
			url : query,
			auth : {
				'bearer' : client_access_token
			}
		}, (err, response) => {
			if (err) reject(err);
			resolve(response);
		});
	});

};

function getTime () {
	var time = new Date().getTime();
	return time = Math.floor(time);
}
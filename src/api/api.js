"use strict";
const request = require('request');
const Bot     = require('../Bot/Bot.js');
const BotController = require('../Bot/BotController');
const LexAnalyzer = require("../LexicalAnalyzer/LexicalAnalyzer.js");

const client_access_token = "ff6078bf82f045de99916465fc02c763";

module.exports = (app) => {
	app.post('/bot', (req, res) => {
		if (!req.body.id || !req.body.query) {
			res.json({
				status : "ERROR",
				errorMessage : "Missed required parameters",
				phrase : []
			});

			return;
		}

		LexAnalyzer.Analyze(req.body.query).then( (result) => {
			if (result.type) {
				let bot = new Bot(result.type, result.entities, req.body.id, req.body.query);
				BotController.addBot(bot);
				let responseResult = bot.form.generatePhrase();
				console.log(bot);
				res.json({
					status : "SUCCESS",
					errorMessage : null,
					result : responseResult,
					isConfirmed : bot.form.isConfirmed,
				});
			} else {
				res.json({
					status : "ERROR",
					errorMessage : "Intent not found"
				});
			}
		});

		/*
		let query = "https://api.api.ai/v1/query?v=20150910&query=" + req.body.query
			+ "&lang=en" + "&sessionId=" + getTime();
		let options = {
			host : query,
			port : "",
			method : "GET"
		};

		let requestP = new Promise( (resolve, reject) => {
			request.get({
				url : query,
				auth : {
					'bearer' : client_access_token
				}
			}, (err, response) => {
				if (err) reject(err);
				resolve(response);
			});
		});

		requestP
		.then( (resp) => {
			let response = JSON.parse(resp.body);
			// let bot = new Bot(response.result.metadata.intentName, )
			console.log(response.result.contexts);
			res.json({
				"API" : true
			});
		})
		.catch( (err) => {
			console.log(err);
		});
		*/
	});

	app.put('/bot', (req, res) => {
		if (!req.body.id || !req.body.query) {
			res.json({
				status : "ERROR",
				errorMessage : "Missed required parameters",
				phrase : []
			});

			return;
		}
		let bot = BotController.findBot(req.body.id);
		//Bot doesn't exist
		if (!bot) {
			res.json({
				status : "ERROR",
				errorMessage : "Invalid bot ID",
				result : "",
				isConfirmed : null
			});

			return;
		}

		let promise = bot.form.fillParameter(req.body.query);
		promise
			.then( (result) => {
				let responseResult = bot.form.generatePhrase();
				console.log(bot.form.params.pointB);
				res.json({
					status : "SUCCESS",
					errorMessage : null,
					result : responseResult,
					isConfirmed : bot.form.isConfirmed,
				});
		})
			.catch(err => {
				console.log(err);
			});
	});

	app.get('/', (req, res) => {
		res.statusCode = 200;
		res.json("OK");
	});
};

function getTime () {
	var time = new Date().getTime();
	return time = Math.floor(time);
}
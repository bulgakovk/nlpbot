"use strict";

const regExps = require("./regExps.js");
const Request = require("../AI/Request.js");

class LexicalAnalyzer {
	static Analyze(string) {
		let found = false, matches = [], intentType = null;
		let matchRegExp = (string) => {
			regExps.forEach( (item) => {
				if (intentType) return;

				let regex = new RegExp(item.regexp, "g");

				while ( (found = regex.exec(string)) !== null) {
					switch (item.type) {
						case "CREATE" : {
							matches.push(this.tryToDeleteLastSpaceSymbols(found[0]));
							item.regexp.lastIndex = found[0].length;
							intentType = "CREATE";
							break;
						}
						case "JOIN" : {
							matches.push(found[1]);
							intentType = "JOIN";
							break;
						}

						case "CANCEL" : {
							matches.push(found[1]);
							intentType = "CANCEL";
							break;
						}

						case "MYTRIPS" : {
							intentType = "MYTRIPS";
							break;
						}

						case "PICK" : {
							intentType = "PICK";
							break;
						}

						case "DELAY" : {
							intentType = "DELAY";
							break;
						}

						case "INFORM" : {
							intentType = "INFORM";
							break;
						}

						case "NOTIFY_ALL" : {
							intentType = "NOTIFY_ALL";
							break;
						}

						default :
							console.log("NOT FOUND");
							break;

					}
				}
			});

			return {
				type     : intentType,
				entities : matches
			}
		};

		return Request(string).then( (response) => {
			let body = JSON.parse(response.body);
			if (body.result.parameters.Time) {
				matches.push(body.result.parameters.Time);
				string = this.tryToDeleteTime(string);
			}

			return matchRegExp(string);
		});
	}

	static isPointA(entity) {
		return /^from (.+(?!from|to|on))$/.test(entity);
	}

	static isPointB(entity) {
		return /^(to|on) (.+(?!from|to|on))$/.test(entity);
	}

	static doesContainPointB(entity) {
		return /(to|on) .+/.test(entity);
	}

	static returnPointB(entity) {
		let found = /(to|on) .+/.exec(entity);
		return found[0];
	}

	static isAgreement(entity) {
		return /(\byes\b|\byeah\b|\bugu\b|\bda\b|\bdefinetely\b|\bexactly\b|\+|\baga\b)/.test(entity);
	}

	static isDeclination(entity) {
		return /\bno\b|\bnot\b|\bNo way\b|\-|\bnet\b/.test(entity);
	}

	static isValidString(entity) {
		return /.*/.test(entity);
	}

	static doesContainsNumber(entity) {
		return /\d+/.test(entity);
	}

	static doesContainID(entity) {
		return /\b\d{1,5}\b(?!:|pm|am)/.test(entity);
	}

	static returnID(entity) {
		let found = /\b\d{1,5}\b(?!:|pm|am)/.exec(entity);

		return found[0];
	}

	static returnNumberFromString(entity) {
		let found;
		found = /\d+/.exec(entity);
		return found[0];
	}

	static tryToDeleteTime(entity) {
		let found = false,
			regexNormalTime = new RegExp(/\d{0,1}\d{0,1}:\d{0,1}\d{0,1}/, "g"),
			regexPreposition = new RegExp(/\bat\b|\bin\b/, "g"),
			regex12HourTime = new RegExp(/((at|in) \d+ (pm|am))|(\d+ (pm|am))/, "g"),
			regexDurationTime = new RegExp(/in \d+ (mins|min|minute|minutes|sec|secs|second|seconds|hours|hour)/, "g");


		entity = this.whileDeletingLoop(entity, regexNormalTime);
		entity = this.whileDeletingLoop(entity, regexPreposition);
		entity = this.whileDeletingLoop(entity, regex12HourTime);
		entity = this.whileDeletingLoop(entity, regexDurationTime);

		return this.tryToDeleteLastSpaceSymbols(entity);
	}

	static whileDeletingLoop(entity, regex) {
		let found = false;

		while(found = regex.exec(entity)) {
			entity = entity.replace(regex, "");
			regex.lastIndex = found[0].length;
		}

		return entity;
	}

	static tryToDeleteLastSpaceSymbols(string) {
		let regex = new RegExp(/\s+$/, "g");
		let found = false;

		while(found = regex.test(string) != false) {
			string = string.replace(regex, "");
		}

		return string;
	}

	static tryToDeleteAllSpaceSymbols(string) {
		let regex = new RegExp(/\s+/, "g");
		let found = false;

		while(found = regex.test(string) != false) {
			string = string.replace(regex, "");
		}

		return string;
	}
}

module.exports = LexicalAnalyzer;


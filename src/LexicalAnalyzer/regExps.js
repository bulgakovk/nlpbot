const regexps = [
	{
		regexp : /\bpick\b/,
		type    : "PICK"
	},
	{
		regexp : /(from|to|on) (((.*)(?=from|to|on))|(.*))/,
		type     : "CREATE"
	},
	{
		regexp   : /\bjoin\b ([1-9]+).*?/,
		type     : "JOIN"
	},
	{
		regexp   : /cancel ([1-9]+).*?/,
		type     : "CANCEL"
	},
	{
		regexp : /\bmytrips\b|\bmy trips\b|\btrips\b/,
		type   : "MYTRIPS"
	},
	{
		regexp : /\bdelay\b/,
		type   : "DELAY"
	},
	{
		regexp : /\binformcrew\b|\binform\b/,
		type   : "INFORM"
	},
	{
		regexp : /\bnotify all\b|\bnotify\b|\bnotify_all\b|\bnotify\b/,
		type   : "NOTIFY_ALL"
	}
];

module.exports = regexps;

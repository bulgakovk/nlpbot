"use strict";

class BotController {
	static addBot(bot) {
		if (this.list === undefined) {
			this.list = new Array(bot);
		} else this.list.push(bot);
	}

	static findBot(id) {
		if (this.list === undefined) return null;

		for (let i = 0; i < this.list.length; i++) {
			if (this.list[i].id === id)
				return this.list[i];
		}

		return null; // If bot doesn't exist
	}


	static getList() {
		return this.list;
	}
}

module.exports = BotController;
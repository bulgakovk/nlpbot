"use strict";
const sha256 = require('sha256');
const salt = "secret_saltasddfghfg";
const FormBuilder = require("../Forms/FormBuilder.js");

class Bot {
	constructor (type, entities, id, query) {
		this.createdAt = getTime();
		this.type = type;
		this.id = id;
		this.form = FormBuilder.buildForm(type, entities, query);
	}
}

function getTime () {
	var time = new Date().getTime();
	return time = Math.floor(time);
}
module.exports = Bot;
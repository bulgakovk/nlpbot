"use strict";
const createIntentBot  = require("./CreateIntentBot");

class Builder {
	static CreateBot(response) {
		switch (response.result.metadata.intentName) {
			case "CREATE" :
				return new createIntentBot(response);
				break;
		}
	}
}

module.exports = Builder;

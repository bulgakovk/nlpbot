"use strict";
const gulp 		  = require('gulp'),
	  babel       = require('gulp-babel'),
	  watch       = require('gulp-watch'),
	  clean       = require('gulp-clean'),
	  runSequence = require('run-sequence'),
      nodemon        = require('gulp-nodemon');


const path = {
	src : 'src/**/*.js',
	dest : 'Build',
	entryPoint : 'Build/app.js'
};

/*
Support tasks
*/
gulp.task("_build", () => {
	return gulp.src(path.src)
		.pipe(babel())
		.pipe(gulp.dest(path.dest));
});
/*
gulp.task("_server", (cb) => {
	nodemon({
		script : path.entryPoint,
		watch  : []
	}).on('restart', () => {
		console.log("Restarting server...");
	});
});
*/
/*
Main tasks
*/

gulp.task('Build', (cb) => {
	runSequence('_build', cb);
});

gulp.task('Watch', (cb) => {
	runSequence('Build');
	gulp.watch(path.src, () => {
		runSequence('Build');
	});
});

gulp.task('Clean', () => {
	return gulp.src(path.dest)
		.pipe(clean());
});
